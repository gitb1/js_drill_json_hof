function calculateTotalSalariesByCountry(data) {
    try {
        const totalSalariesByCountry = data.reduce((accumulator, person) => {
            const country = person.location;
            const salary = parseFloat(person.salary.replace('$', ''));
            
            
            // If the country already exists in the accumulator, add the salary to its total
            if (accumulator[country]) {
                accumulator[country] += salary;
            } else {
                // If the country doesn't exist, initialize it with the current salary
                accumulator[country] = salary;
            }

            return accumulator;
        }, {});

        return totalSalariesByCountry;
    } catch (error) {
        console.error('An error occurred:', error);
        return null; // or any other value that indicates error
    }
}


module.exports = calculateTotalSalariesByCountry;

