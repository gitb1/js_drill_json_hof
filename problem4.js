function calculateTotalSalary(data) {
    try {
        const totalSalary = data.reduce((accumulator, person) => {
            const salary = parseFloat(person.salary.replace('$', ''));
            return accumulator + salary;
        }, 0);
        
        return totalSalary;

       
    } catch (error) {
        console.error('An error occurred:', error);
        return null; // or any other value that indicates error
    }
}

module.exports = calculateTotalSalary;
