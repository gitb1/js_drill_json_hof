function calculateAverageSalaryByCountry(data) {
    try {

        const { totalSalariesByCountry, countByCountry } = data.reduce((acc, person) => {
            const { location, salary } = person;
            const country = location;
            const parsedSalary = parseFloat(salary.replace('$', ''));
            

            acc.totalSalariesByCountry[country] = (acc.totalSalariesByCountry[country] || 0) + parsedSalary;
            acc.countByCountry[country] = (acc.countByCountry[country] || 0) + 1;

            return acc;
        }, { totalSalariesByCountry: {}, countByCountry: {} });

        const averageSalariesByCountry = Object.keys(totalSalariesByCountry).reduce((acc, country) => {
            acc[country] = totalSalariesByCountry[country] / countByCountry[country];
            return acc;
        }, {});

        return averageSalariesByCountry;
    } catch (error) {
        console.error('An error occurred:', error);
        return null; // or any other value that indicates error
    }
}

module.exports = calculateAverageSalaryByCountry;
