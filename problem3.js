function convertAndCorrectSalaries(data) {
    try {
        const newData = data.map(person => {
            const salary = parseFloat(person.salary.replace('$', ''));
            const correctedSalary = salary * 10000;
            return {
                ...person,
                corrected_salary: correctedSalary
            };
            
        });
        return newData;
    } catch (error) {
        console.error('An error occurred:', error);
        return null; // or any other value that indicates error
    }
}

module.exports = convertAndCorrectSalaries;
