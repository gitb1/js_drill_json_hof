//  Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).

try {
    const calculateTotalSalariesByCountry = require('../problem5.js');
    // Sample data
    const data = require('../js_drill_2.cjs');

    // Test the calculateTotalSalariesByCountry function
    const totalSalariesByCountry = calculateTotalSalariesByCountry(data);

    // Check the total salaries by country
    console.log('Total Salaries by Country:', totalSalariesByCountry);
} catch (error) {
    console.error('An error occurred:', error);
}