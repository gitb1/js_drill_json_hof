//Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)
   

try {
    const convertAndCorrectSalaries= require('../problem3.js');
    
   
  // Read the contents of the .cjs file
  const data = require('../js_drill_2.cjs');

  const correctedData = convertAndCorrectSalaries(data);

// Check if salaries are converted to numbers and corrected
   console.log(correctedData);
} catch (error) {
    console.error('Error:', error.message);
}