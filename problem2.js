
function convertSalariesToNumbers(data) {

 try{
    
    const newData = data.map(person => {
      return {
        ...person,
        salary: parseFloat(person.salary.replace('$', ''))
      };
    });
    return newData;
 
  }catch(error){
    console.error('Error:',error.message);
  }
}


module.exports = convertSalariesToNumbers;