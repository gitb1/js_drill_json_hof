
function findWebDevelopers(data) {
try{
  
  const webDevelopers = data.filter(person => {
    return person.job && person.job.toLowerCase().includes('web developer');
  });

  return webDevelopers;

  }catch(error){
       console.error('Error:', error.message);
  }
}
module.exports = findWebDevelopers;